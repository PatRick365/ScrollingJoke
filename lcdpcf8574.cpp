#include "lcdpcf8574.h"

#include "wiringpi.h"

#include <iostream>

#include <wiringPi.h>
#include <pcf8574.h>
#include <lcd.h>

namespace LcdPcf8574PiConst
{
constexpr int PCF8574_ADDRESS = 0x27;

constexpr int BASE = 64;
constexpr int  RS = BASE + 0;
constexpr int  RW = BASE + 1;
constexpr int  EN = BASE + 2;
constexpr int LED = BASE + 3;
constexpr int  D4 = BASE + 4;
constexpr int  D5 = BASE + 5;
constexpr int  D6 = BASE + 6;
constexpr int  D7 = BASE + 7;

constexpr int  LCD_WIDTH = 16;
constexpr int  LCD_HEIGHT = 2;
}

using namespace LcdPcf8574PiConst;

LcdPcf8574::LcdPcf8574()
    : ComponentBase("PCF8574"),
      m_lcdHandle(-1)
{
    if (!init())
    {
        std::cerr << "init of " << m_name << " failed !\n";
    }
}

void LcdPcf8574::setPosition(unsigned int x, unsigned int y)
{
    //std::cout << "pos: " << x << "," << y << "\n";
    lcdPosition(m_lcdHandle, x, y);     // set the LCD cursor position to (0,0)
}

void LcdPcf8574::print(const std::string &text)
{
    //std::cout << "printing: '" << text << "'\n";
    lcdPrintf(m_lcdHandle, text.c_str());   // Display CPU temperature on LCD
}

void LcdPcf8574::print(const std::string &text, unsigned int x, unsigned int y)
{
    setPosition(x, y);
    print(text);
}

void LcdPcf8574::clear()
{
    print(std::string(LCD_WIDTH * LCD_HEIGHT, ' '), 0, 0);
}

bool LcdPcf8574::init()
{
    if (!WiringPi::instance().isInit())
    {
        if (!WiringPi::instance().init())
        {
            return false;
        }
    }

    pcf8574Setup(BASE, PCF8574_ADDRESS);    // initialize PCF8574

    for (int i = 0; i < 8; i++)
    {
        pinMode(BASE + i, OUTPUT);     // set PCF8574 port to output mode
    }
    digitalWrite(LED, HIGH);      // turn on LCD backlight
    digitalWrite(RW, LOW);        // allow writing to LCD

    m_lcdHandle = lcdInit(2, 16, 4, RS, EN, D4, D5, D6, D7, 0, 0, 0, 0);  // initialize LCD and return “handle” used to handle LCD
    if(m_lcdHandle == -1)
    {
        std::cerr << "lcdInit failed !\n";
        return false;
    }
    m_isInit = true;
    return true;
}
