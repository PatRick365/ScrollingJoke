#include "application.h"

#include "wiringpi.h"
#include "logger.h"

Application::Application()
    : m_jokeDownloaderBuffer("https://official-joke-api.appspot.com/random_joke", 3)
{
    LOG(Logger::eDebug, "Application start...");
}

bool Application::init()
{
    WiringPi::instance().registerInput(WiringPi::Gpio::eGpio18, std::bind(&Application::onButtonJokePress, this));

    //m_jokeDownloadbuffer.run()

    return true;
}

int Application::run()
{
    m_scrollingLcd.run();
    m_jokeDownloaderBuffer.run();

    while (1)
    {

    }
    return 0;
}

void Application::onButtonJokePress()
{
    if (m_scrollingLcd.isIdle())
    {
        if (!m_currentJoke || !m_currentJoke->hasNext())
        {
            if (m_jokeDownloaderBuffer.size() > 0)
            {
                m_currentJoke = std::make_unique<JsonJoke>(m_jokeDownloaderBuffer.pop());
            }
            else
            {
                LOG(Logger::eImportant, "No JsonJoke available");
                return;
            }
        }
        m_scrollingLcd.setTextUpper(m_currentJoke->next());
    }
}

