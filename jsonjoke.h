#pragma once

#include "jsonmultisentenceparsebase.h"

class JsonJoke : public JsonMultiSentenceParseBase
{
public:
    JsonJoke(const nlohmann::json &json);
};
