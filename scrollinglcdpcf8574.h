#pragma once

#include "lcdpcf8574.h"
#include "scrollingtext.h"

#include <thread>
#include <memory>

class ScrollingLcdPcf8574 : public LcdPcf8574
{
public:
    ScrollingLcdPcf8574(unsigned int width = 16);

    void run();

    void setTextUpper(const std::string &text);
    void setTextLower(const std::string &text);

    bool isIdle() const;


private:
    unsigned int m_width;

    ScrollingText m_textUpper;
    ScrollingText m_textLower;

    std::unique_ptr<std::thread> m_renderThread;

};
