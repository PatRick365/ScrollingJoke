#include "jsondownloaderbuffer.h"
#include "logger.h"

#include <fmt/format.h>

#include <chrono>
#include <iostream>

#include <curl/curl.h>

using namespace std::chrono_literals;

size_t curlWriteCallback(void *contents, size_t size, size_t nmemb, std::string *str);

JsonDownloaderBuffer::JsonDownloaderBuffer(const std::string &url, size_t bufferSize)
    : m_url(url),
      m_bufferSize(bufferSize),
      m_isDownloadThreadRunning(false),
      m_quitFlag(false)
{}

JsonDownloaderBuffer::~JsonDownloaderBuffer()
{
    if (m_isDownloadThreadRunning)
    {
        m_quitFlag = true;
        m_downloadThread->join();
    }
}

void JsonDownloaderBuffer::run()
{
    startDownloadThread();
}

nlohmann::json JsonDownloaderBuffer::pop()
{
    std::lock_guard<std::mutex> lock(m_bufferAccessMutex);

    auto json = m_jsonBuffer.front();

    m_jsonBuffer.pop_front();

    return json;
}

size_t JsonDownloaderBuffer::size() const
{
    return m_jsonBuffer.size();
}

void JsonDownloaderBuffer::startDownloadThread()
{
    m_downloadThread = std::make_unique<std::thread>([this]()
    {
        m_isDownloadThreadRunning = true;

        CURL *curl;
        CURLcode res;

        curl_global_init(CURL_GLOBAL_DEFAULT);

        curl = curl_easy_init();

        std::string str;

        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, m_url.c_str());
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str);
        }
        else
        {
            std::cerr << __PRETTY_FUNCTION__ << " SHIT !!!\n";
            return;
        }

        while (!m_quitFlag)
        {
            //std::lock_guard<std::mutex> lock(m_bufferAccessMutex); or here ??

            if (m_jsonBuffer.size() < m_bufferSize)
            {
                LOG(Logger::eDebug, fmt::format("Downloading... {} of {}", m_jsonBuffer.size() + 1, m_bufferSize));

                res = curl_easy_perform(curl);

                if(res != CURLE_OK)
                {
                    std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << "\n";
                }

                {
                std::lock_guard<std::mutex> lock(m_bufferAccessMutex);
                m_jsonBuffer.push_back(nlohmann::json::parse(str));
                }

                str.clear();
            }

            std::this_thread::sleep_for(5s);
        }

        curl_easy_cleanup(curl);
    });
}

size_t curlWriteCallback(void *contents, size_t size, size_t nmemb, std::string *str)
{
    size_t newLength = size * nmemb;
    try
    {
        str->append((char*)contents, newLength);
    }
    catch(std::bad_alloc &e)
    {
        std::cout << "where: " << __PRETTY_FUNCTION__ << " - what: " << e.what() << std::endl;
        abort();
    }
    return newLength;
}
