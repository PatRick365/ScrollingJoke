#pragma once

#include <functional>
#include <unordered_map>
#include <thread>
#include <memory>

namespace WiringPiConst
{
}

class WiringPi
{
public:
    enum Gpio
    {
        eGpio17 = 0,
        eGpio18 = 1,
        eGpio27 = 2,
        eGpio22 = 3,
        eGpio23 = 4,
        eGpio24 = 5,
        eGpio25 = 6,
        eGpio4 = 7,
        eGPio2_SDA = 8,
        eGpio3_SCL = 9
    };


private:
    WiringPi(); // Disallow instantiation outside of the class.


public:
    WiringPi(const WiringPi&) = delete;
    WiringPi& operator=(const WiringPi &) = delete;
    WiringPi(WiringPi &&) = delete;
    WiringPi & operator=(WiringPi &&) = delete;

    static WiringPi& instance();

    bool init();
    bool initPcf8574();

    bool isInit() const;

    void startInputThread();

    void registerInput(WiringPi::Gpio pin, const std::function<void ()> &callback);


private:
    bool m_isInit;

    std::unordered_map<WiringPi::Gpio, std::function<void ()>> m_callbacks;

    //std::atomic_bool m_quitFlag;
    std::unique_ptr<std::thread> m_inputThread;
    bool m_isThreadRunning;
};
