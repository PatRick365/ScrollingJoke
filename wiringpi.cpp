#include "wiringpi.h"

#include <iostream>

#include <wiringPi.h>

namespace WiringPiConst
{

}

using namespace WiringPiConst;

WiringPi::WiringPi()
    : m_isInit(false),
      m_isThreadRunning(false)
{}

WiringPi &WiringPi::instance()
{
    static WiringPi wiringPi;
    return wiringPi;
}

bool WiringPi::init()
{
    // mutex maybe
    if (m_isInit)
    {
        std::cerr << "wiringPi already setup!\n";
        return true;
    }

    m_isInit = (wiringPiSetup() > -1);

    if (!m_isInit)
        std::cerr << "setup wiringPi failed!\n";

    return m_isInit;
}

bool WiringPi::isInit() const
{
    return m_isInit;
}

void WiringPi::startInputThread()
{
    m_isThreadRunning = true;

    m_inputThread = std::make_unique<std::thread>([this]()
    {
        for (;;)
        {
            for (const auto &callback : m_callbacks)
            {
                if(digitalRead(static_cast<int>(callback.first)) == LOW)
                {
                    callback.second();
                }
            }
        }
    });
}

void WiringPi::registerInput(WiringPi::Gpio pin, const std::function<void ()> &callback)
{
    if (!m_isInit)
    {
        init();
    }

    m_callbacks[pin] = callback;

    pinMode(static_cast<int>(pin), INPUT);

    if (!m_isThreadRunning)
    {
        startInputThread();
    }
}


