#include "logger.h"

#include <iostream>
#include <chrono>
#include <ctime>
#include <sys/time.h>

Logger::Logger()
    : m_logLevel(LogLevel::eDebug)
{

}

Logger &Logger::instance(){
    static Logger Logger;
    return Logger;
}

void Logger::log(Logger::LogLevel logLevel, std::string text, std::thread::id threadId, const char *file, int line)
{
    if (static_cast<int>(logLevel) < static_cast<int>(m_logLevel))
        return;

    /*auto time = std::chrono::system_clock::now();
    time_t tt = std::chrono::system_clock::to_time_t(time);

    //tm utc_tm = *gmtime(&tt);
    tm local_tm = *localtime(&tt);

    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];*/

    timeval curTime;
    gettimeofday(&curTime, nullptr);
    int milli = static_cast<int>(curTime.tv_usec / 1000);

    char buffer [80];
    strftime(buffer, 80, "%H:%M:%S", localtime(&curTime.tv_sec));

    char currentTime[84] = "";
    sprintf(currentTime, "%s.%d", buffer, milli);

    /*
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    //strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
    strftime(buffer,sizeof(buffer),"%H:%M:%S",timeinfo);*/
    std::string str(currentTime);

    //std::cout << "[" << local_tm.tm_hour << ":" << local_tm.tm_min << ":" << local_tm.tm_sec << "] "

    std::cout << "[" << str << "] "
              << "[" << threadId << "] "
              << text << " - "
              << file
              << ":"
              << line
              << std::endl;
              //<< "\n";

}

Logger::LogLevel Logger::logLevel() const
{
    return m_logLevel;
}

void Logger::setLogLevel(const Logger::LogLevel &logLevel)
{
    m_logLevel = logLevel;
}
