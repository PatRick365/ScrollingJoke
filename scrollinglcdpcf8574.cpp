#include "scrollinglcdpcf8574.h"

#include <chrono>
#include <iostream>

using namespace std::chrono_literals;

ScrollingLcdPcf8574::ScrollingLcdPcf8574(unsigned int width)
    : LcdPcf8574(),
      m_width(width)
{}

void ScrollingLcdPcf8574::run()
{
    m_renderThread = std::make_unique<std::thread>([this]()
    {
        while (1)
        {
            if (!m_textUpper.isAtEnd())
            {
                print(m_textUpper.text(), 0, 0);
                ++m_textUpper;
            }

            if (!m_textLower.isAtEnd())
            {
                print(m_textLower.text(), 0, 1);
                ++m_textLower;
            }

            std::this_thread::sleep_for(300ms);
        }
    });
}

void ScrollingLcdPcf8574::setTextUpper(const std::string &text)
{
    m_textUpper = ScrollingText(text, m_width);
}

void ScrollingLcdPcf8574::setTextLower(const std::string &text)
{
    m_textLower = ScrollingText(text, m_width);
}

bool ScrollingLcdPcf8574::isIdle() const
{
    return (m_textUpper.isAtEnd() && m_textLower.isAtEnd());
}



/*



    std::string str1(m_text.begin(), m_text.begin() + static_cast<size_t>(LCD_WIDTH));
    std::string str2(m_text.begin() + static_cast<size_t>(LCD_WIDTH), m_text.end());

    str1.insert(str1.size(), static_cast<size_t>(LCD_WIDTH) - str1.size(), ' ');
    str2.insert(str2.size(), static_cast<size_t>(LCD_WIDTH) - str2.size(), ' ');

    std::list<char> q1, q2;
    std::copy(str1.begin(), str1.end(), std::back_inserter(q1));
    std::copy(str2.rbegin(), str2.rend(), std::back_inserter(q2));

    while (1)
    {
        std::string sss1, sss2;

        std::copy(q1.begin(), q1.end(), std::back_inserter(sss1));
        std::copy(q2.begin(), q2.end(), std::back_inserter(sss2));

        print(sss1, 0, 0);
        print(sss2, 0, 1);

        q2.push_back(q1.back());
        q1.pop_back();

        q1.push_front(q2.front());
        q2.pop_front();

        delay(500);
    }

str
1 take back, append to 2 back
2 take front, push_front1

0123456789
9876543210

9012345678
8765432109


*/
