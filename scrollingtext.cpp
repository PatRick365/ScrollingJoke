#include "scrollingtext.h"

ScrollingText::ScrollingText(const std::string &text, unsigned int width)
    : m_width(width),
      m_pos(1),
      m_text(text)
{
    m_text.insert(0, width, ' ');
}

ScrollingText::ScrollingText()
    : m_width(0),
    m_pos(1)
{

}

int ScrollingText::pos() const
{
    return m_pos;
}

bool ScrollingText::isAtEnd() const
{
    return static_cast<unsigned int>(m_pos) > m_text.size();
}

std::string ScrollingText::text() const
{
    std::string str;

    if (!isAtEnd())
        str = m_text.substr(static_cast<unsigned int>(m_pos), m_width);

    if(str.size() < m_width)
        str.insert(str.size(), m_width - str.size(), ' ');

    return str;
}


ScrollingText& ScrollingText::operator++()
{
   m_pos++;
   return *this;
}

ScrollingText ScrollingText::operator++(int)
{
   ScrollingText temp = *this;
   ++*this;
   return temp;
}
