#include "jsonmultisentenceparsebase.h"

JsonMultiSentenceParseBase::JsonMultiSentenceParseBase(const nlohmann::json &json)
    : m_json(json),
      m_index(0)
{

}

JsonMultiSentenceParseBase::~JsonMultiSentenceParseBase() = default;

bool JsonMultiSentenceParseBase::hasNext() const
{
    return m_index < m_parts.size();
}

std::string JsonMultiSentenceParseBase::next()
{
    return m_parts.at(m_index++);
}


