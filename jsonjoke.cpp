#include "jsonjoke.h"

#include "logger.h"

#include <fmt/format.h>


// format:
// {"id":98,"punchline":"Now we just have to call him Dav.","setup":"Did you hear that David lost his ID in prague?","type":"general"}


JsonJoke::JsonJoke(const nlohmann::json &json)
    : JsonMultiSentenceParseBase(json)
{
    m_parts = { json.at("setup"), json.at("punchline") };

    LOG(Logger::eDebug, fmt::format("Getting new JsonJoke object\n\ttype: \"{}\" \n\tsetup: \"{}\" \n\tpunchline: \"{}\"", json.at("type"), json.at("setup"), json.at("punchline") ));
}
