﻿#pragma once

#include <string>
#include <thread>

#define LOG(logLevel, text) Logger::instance().log(logLevel, text, std::this_thread::get_id(), __FILE__, __LINE__)

class Logger
{
public:
    enum LogLevel
    {
        eNone = -1,
        eImportant,
        eDebug,
        eFull
    };

private:
    Logger(); // Disallow instantiation outside of the class.


public:
    Logger(const Logger&) = delete;
    Logger& operator=(const Logger &) = delete;
    Logger(Logger &&) = delete;
    Logger & operator=(Logger &&) = delete;

    static Logger &instance();

    void log(Logger::LogLevel logLevel, std::string text, std::thread::id threadId, const char *file, int line);


    Logger::LogLevel logLevel() const;
    void setLogLevel(const Logger::LogLevel &logLevel);


private:
    Logger::LogLevel m_logLevel;
};
