#include <iostream>

#include "application.h"

int main()
{
    Application app;

    if (!app.init())
    {
        std::cerr << "Application::init failed..." << std::endl;
    }

    return app.run();
}
