#pragma once

#include "json.hpp"
#include <vector>
#include <string>

class JsonMultiSentenceParseBase
{
public:
    JsonMultiSentenceParseBase(const nlohmann::json &json);

    ///
    /// \todo make pure virtual ?
    ///
    virtual ~JsonMultiSentenceParseBase();

    bool hasNext() const;

    std::string next();


protected:
    nlohmann::json m_json;
    std::vector<std::string> m_parts;


private:
    size_t m_index;
};
