#pragma once

#include <string>

class ScrollingText
{
public:
    ScrollingText(const std::string &text, unsigned int width);
    ScrollingText();

    int pos() const;

    bool isAtEnd() const;

    std::string text() const;

    ScrollingText &operator++();
    ScrollingText operator++(int);


private:
    unsigned int m_width;
    int m_pos;
    std::string m_text;
};
