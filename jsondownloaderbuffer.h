#pragma once

#include <memory>
#include <thread>
#include <atomic>
#include <mutex>
#include <list>
#include <string>

#include "json.hpp"

class JsonDownloaderBuffer
{
public:
    JsonDownloaderBuffer(const std::string &url, size_t bufferSize);

    ~JsonDownloaderBuffer();

    void run();

    nlohmann::json pop();

    size_t size() const;


private:
    void startDownloadThread();


private:
    std::string m_url;
    size_t m_bufferSize;

    bool m_isDownloadThreadRunning;
    std::unique_ptr<std::thread> m_downloadThread;
    std::atomic_bool m_quitFlag;
    std::mutex m_bufferAccessMutex;


    std::list<nlohmann::json> m_jsonBuffer;
};
