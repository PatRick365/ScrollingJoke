TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        application.cpp \
        componentbase.cpp \
        jsondownloaderbuffer.cpp \
        jsonjoke.cpp \
        jsonmultisentenceparsebase.cpp \
        lcdpcf8574.cpp \
        logger.cpp \
        main.cpp \
        pin.cpp \
        scrollinglcdpcf8574.cpp \
        scrollingtext.cpp \
        wiringpi.cpp

HEADERS += \
    application.h \
    componentbase.h \
    curlget.h \
    json.hpp \
    jsondownloaderbuffer.h \
    jsonjoke.h \
    jsonmultisentenceparsebase.h \
    lcdpcf8574.h \
    logger.h \
    scrollinglcdpcf8574.h \
    scrollingtext.h \
    wiringpi.h

unix:!macx: LIBS += -lfmt
