#pragma once

#include "scrollinglcdpcf8574.h"
#include "jsondownloaderbuffer.h"
#include "jsonjoke.h"

#include <memory>

class Application
{
public:
    Application();

    bool init();

    int run();


private:
    void onButtonJokePress();


private:
    ScrollingLcdPcf8574 m_scrollingLcd;
    JsonDownloaderBuffer m_jokeDownloaderBuffer;

    std::unique_ptr<JsonJoke> m_currentJoke;
};
