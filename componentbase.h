#pragma once

#include <string>

class ComponentBase
{
public:
    ComponentBase(const std::string &name);

    virtual ~ComponentBase() = default;

    bool isInit() const;


private:
    virtual bool init() = 0;


protected:
    bool m_isInit;
    std::string m_name;
};
