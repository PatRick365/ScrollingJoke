#include "componentbase.h"

ComponentBase::ComponentBase(const std::string &name)
    : m_isInit(false),
      m_name(name)
{}

bool ComponentBase::isInit() const
{
    return m_isInit;
}
