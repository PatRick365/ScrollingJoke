#pragma once

#include "componentbase.h"

#include <string>

namespace LcdPcf8574PiConst
{
extern const int PCF8574_ADDRESS;

extern const int BASE;
extern const int RS;
extern const int RW;
extern const int EN;
extern const int LED;
extern const int D4;
extern const int D5;
extern const int D6;
extern const int D7;

extern const int LCD_WIDTH;
extern const int LCD_HEIGHT;
}

class LcdPcf8574 : public ComponentBase
{
public:
    LcdPcf8574();

    void setPosition(unsigned int x, unsigned int y);

    void print(const std::string &text);
    void print(const std::string &text, unsigned int x, unsigned int y);

    void clear();


private:
    bool init() override;


private:
    int m_lcdHandle;
};
